#+TITLE: Search Engine Comparison
#+HTML_HEAD: <link rel='stylesheet' type='text/css' href='styles.css' />
#+OPTIONS: html-style:nil toc:nil num:nil html-postamble:nil

* What are they

|----------------+---------------+-----------------------------------------------------------+------------------|
|                | Google        | DuckDuckGo                                                | Searx            |
|----------------+---------------+-----------------------------------------------------------+------------------|
| Code           | ~Proprietary~ | Part of it is ~Libre Software~, but core is ~proprietary~ | ~Libre Software~ |
|----------------+---------------+-----------------------------------------------------------+------------------|
| Decentralized? | ~No~          | ~No~                                                      | ~Yes~            |
|----------------+---------------+-----------------------------------------------------------+------------------|

* Searx is looking spicy
  As you can see in the two categories Searx wins them all! to read more info about the project take a look at.
  1. https://github.com/searx/searx
  2. https://searx.github.io/searx/

* To add to the Searx awesomeness: INSTANCES
  Searx allows people to host searx on instances. Kind of like librarian/invidious. This makes Searx decentralized. More then just one person is in control.

* Instances available
  A list of all instances can be found at: https://searx.space/.
  Here are some instances that I like.

  1. https://matthewevan.xyz/searx
  2. https://anon.sx
    
* How do I set it up in my browser?
  That's kind of more complicated then it should be because Searx isn't a search engine that browsers have by default.
  
  Luckily, in Chromium-based browsers it's kind of simple to do:

  1. Click on the hamburger menu
  2. Go to settings
  3. In settings find something labeled search engine
  4. Click on manage search engines
  5. Find other search engines
  6. Click Add
  7. There will be three options ~Search Engine~, ~Keyword~ and ~URL with %s in place of query~
  8. In search engine for this tutorial I am going to teach how to add https://matthewevan.xyz/searx
  9. For ~Search Engine~ type https://matthewevan.xyz/searx
  10. For ~Keyword~ type matthewevan.xyz/searx
  11. For ~URL with %s in place of query~ type this: https://matthewevan.xyz/searx/search?q=%s&categories=general&language=en-US
  12. Finally click ~Save~!

  To add it in a Firefox-based browser under version 89 (before the Proton UI change; includes GNU Icecat) use the following steps:
 
  1. Go to the Searx instance you want to add.
  2. Click on the "Page Actions" menu, the three horizontal dots in the URL bar.
  3. Click the "Add Search Engine" button
  4. (Optional) Go to the "Search" tab in ~about:preferences~ and change the Default Search Engine to the instance you just added.

In Firefox versions 89+ instead of going into the Page Actions menu, right click on the URL bar and click the ~Add "<your-instance.com>"~ button.

* Bye Bye
  If everything went good you now have searx as your default search engine. Enjoy!
