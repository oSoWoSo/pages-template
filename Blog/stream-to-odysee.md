As of now [Odysee](https://odysee.com) allows for live streaming. Here is how to do it.

1. **Donate** 100LBC to your self. Not a video, but your actual channel. 
2. **Find livestream** in your `uploads` tab when clicking the `upload` button you should see `Livestream` as one of the options.
3. **Fill out info** for the publication and click upload.
4. **Get your stream key** You should see now that odysee will have given you a stream key. **DO NOT SHARE TO ANYONE** you should also see a server name.
5. [OBS](obsproject.com) with this program you will be able to stream to odysee. As far as I know there are no other ways to do it.
6. Go to `file > settings > stream` in the `service` do custom, in the Server to `rtmp://stream.odysee.com/live` and in the stream key put in your stream key.
7. Click `apply` and `ok` and you should be good to go.

# Possible feature of fastlbry
On a command it will start streaming. Maybe this can be done with ffmpeg.
